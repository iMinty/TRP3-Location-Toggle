## Interface: 70300
## Title: TRP3 Location Toggle
## Version: 1.1
## Notes: Adds a toolbar button to quickly enable/disable map location for TotalRP 3.
## Author: Lorrissa-ArgentDawn (EU)
## Dependencies: TotalRP3

TRP3LocationToggle.lua